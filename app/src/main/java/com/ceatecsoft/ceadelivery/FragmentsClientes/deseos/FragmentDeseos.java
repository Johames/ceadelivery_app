package com.ceatecsoft.ceadelivery.FragmentsClientes.deseos;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.ceatecsoft.ceadelivery.R;

public class FragmentDeseos extends Fragment {

    public FragmentDeseos(){};

    @SuppressLint("NewAPi")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_clientes_deseos, container, false);

        return root;
    }
}