package com.ceatecsoft.ceadelivery.FragmentsTiendas.Promociones;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ceatecsoft.ceadelivery.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

public class FragmentPromociones extends Fragment {

    private Dialog dialogAgregarPromocion;

    private SwipeRefreshLayout RefreshPromocionesActivas, RefreshPromocionesCaducadas;

    String tipo = "activas";

    private ListView ListaPromocionesActivas, ListaPromocionesCaducadas;

    @SuppressLint({"UseRequireInsteadOfGet", "NewApi"})
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tiendas_promociones, container, false);

        setHasOptionsMenu(true);

        dialogAgregarPromocion = new Dialog(Objects.requireNonNull(getActivity()));

        final Button PromActivas = root.findViewById(R.id.PromActivas);
        final Button PromCaducadas = root.findViewById(R.id.PromCaducadas);

        ListaPromocionesActivas = root.findViewById(R.id.ListaPromocionesActivas);
        ListaPromocionesActivas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        ListaPromocionesCaducadas = root.findViewById(R.id.ListaPromocionesCaducadas);
        ListaPromocionesCaducadas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        RefreshPromocionesActivas = root.findViewById(R.id.RefreshPromocionesActivas);
        RefreshPromocionesCaducadas = root.findViewById(R.id.RefreshPromocionesCaducadas);

        PromActivas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipo = "activas";

                PromActivas.setBackgroundResource(R.drawable.fondo_border_left_default_ripple);
                PromActivas.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                PromCaducadas.setBackgroundResource(R.drawable.fondo_border_right_ripple);
                PromCaducadas.setTextColor(getResources().getColor(R.color.colorWhite));

                RefreshPromocionesActivas.setVisibility(View.VISIBLE);
                RefreshPromocionesCaducadas.setVisibility(View.GONE);
            }
        });

        PromCaducadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipo = "caducadas";

                PromActivas.setBackgroundResource(R.drawable.fondo_border_left_ripple);
                PromActivas.setTextColor(getResources().getColor(R.color.colorWhite));

                PromCaducadas.setBackgroundResource(R.drawable.fondo_border_right_default_ripple);
                PromCaducadas.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                RefreshPromocionesActivas.setVisibility(View.GONE);
                RefreshPromocionesCaducadas.setVisibility(View.VISIBLE);
            }
        });

        RefreshPromocionesActivas.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light);

        RefreshPromocionesActivas.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //
                    }
                }, 1000);
            }
        });

        RefreshPromocionesCaducadas.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light);

        RefreshPromocionesCaducadas.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //
                    }
                }, 1000);
            }
        });

        FloatingActionButton AddPromocion = root.findViewById(R.id.AddPromocion);
        AddPromocion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AgregarPromocion();
            }
        });

        return root;
    }

    @SuppressLint("NewApi")
    private void AgregarPromocion(){
        dialogAgregarPromocion.setContentView(R.layout.modal_agregar_promocion);
        dialogAgregarPromocion.setCancelable(false);


        Button btnCerrar = dialogAgregarPromocion.findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAgregarPromocion.dismiss();
            }
        });

        Button btnAplicar = dialogAgregarPromocion.findViewById(R.id.btnAplicar);
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        Objects.requireNonNull(dialogAgregarPromocion.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAgregarPromocion.show();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem home = menu.findItem(R.id.item_home);
        home.setVisible(false);
        MenuItem promo = menu.findItem(R.id.item_promociones);
        promo.setEnabled(false);
        MenuItem prod = menu.findItem(R.id.item_productos);
        prod.setVisible(false);
        MenuItem pedido = menu.findItem(R.id.item_pedidos);
        pedido.setVisible(false);
        MenuItem ventas = menu.findItem(R.id.item_ventas);
        ventas.setVisible(false);
        MenuItem config = menu.findItem(R.id.item_configuracion);
        config.setVisible(false);
        MenuItem valor = menu.findItem(R.id.item_valoracion);
        valor.setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }







}