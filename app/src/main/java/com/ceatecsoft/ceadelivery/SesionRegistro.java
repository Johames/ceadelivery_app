package com.ceatecsoft.ceadelivery;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Bundle;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.ebanx.swipebtn.SwipeButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityOptionsCompat;

import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.transition.Explode;
import android.transition.Transition;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

public class SesionRegistro extends AppCompatActivity {

    SharedPreferences Preferences, datosUsuario, guiaUsuarios;

    Dialog alertPermisos;

    String modo = "", save = "", dtoUser = "", dtoPass = "";
    boolean ver = false;

    private static final long DURATION_TRANSACTION = 500;

    LinearLayout ContentModo;
    RelativeLayout ContentInicioSesion, ContentRegistro;

    CardView IngresarInvitado, IngresarCliente, IngresarTienda, Registrarse;

    ImageButton btnCerrarRegistro;




    TextView TituloInicio;
    ImageButton btnCerrar;
    EditText Usuario, Contrasena;
    ImageButton VerContrasena;
    CheckBox Recordar;
    SwipeButton IniciarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sesion_registro);

        alertPermisos = new Dialog(this);

        Preferences = getSharedPreferences("IncoCtaUso", Context.MODE_PRIVATE);
        datosUsuario = getSharedPreferences("DtsCtaUsr", Context.MODE_PRIVATE);
        guiaUsuarios = getSharedPreferences("GuiaUsers", Context.MODE_PRIVATE);

        ContentModo = findViewById(R.id.ContentModo);
        ContentInicioSesion = findViewById(R.id.ContentInicioSesion);
        ContentRegistro = findViewById(R.id.ContentRegistro);

        IngresarInvitado = findViewById(R.id.IngresarInvitado);
        IngresarCliente = findViewById(R.id.IngresarCliente);
        IngresarTienda = findViewById(R.id.IngresarTienda);
        Registrarse = findViewById(R.id.Registrarse);

        btnCerrarRegistro = findViewById(R.id.btnCerrarRegistro);


        TituloInicio = findViewById(R.id.TituloInicio);
        btnCerrar = findViewById(R.id.btnCerrar);
        Usuario = findViewById(R.id.Usuario);
        Contrasena = findViewById(R.id.Contraseña);
        VerContrasena = findViewById(R.id.VerContraseña);
        Recordar = findViewById(R.id.Recordar);
        IniciarSesion = findViewById(R.id.IniciarSesion);

        IngresarInvitado.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                Transition entrar = new Explode();
                entrar.setDuration(DURATION_TRANSACTION);
                entrar.setInterpolator(new AccelerateInterpolator());

                getWindow().setExitTransition(entrar);

                Intent cliente = new Intent(getApplicationContext(), PrincipalClientes.class);
                startActivity(cliente, ActivityOptionsCompat.makeSceneTransitionAnimation(SesionRegistro.this).toBundle());

                SesionRegistro.this.finish();
            }
        });

        IngresarCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modo = "Ingresar como Cliente";
                ContentModo.setVisibility(View.GONE);
                ContentInicioSesion.setVisibility(View.VISIBLE);
                ContentRegistro.setVisibility(View.GONE);
                TituloInicio.setText(modo);
            }
        });

        IngresarTienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modo = "Ingresar como Tienda";
                ContentModo.setVisibility(View.GONE);
                ContentInicioSesion.setVisibility(View.VISIBLE);
                ContentRegistro.setVisibility(View.GONE);
                TituloInicio.setText(modo);
            }
        });

        Registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modo = "";
                ContentModo.setVisibility(View.GONE);
                ContentInicioSesion.setVisibility(View.GONE);
                ContentRegistro.setVisibility(View.VISIBLE);
                TituloInicio.setText("");

                OtorgarPermisos();
            }
        });



        btnCerrarRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modo = "";
                ContentModo.setVisibility(View.VISIBLE);
                ContentInicioSesion.setVisibility(View.GONE);
                ContentRegistro.setVisibility(View.GONE);
                TituloInicio.setText("");
            }
        });












        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modo = "";
                ContentModo.setVisibility(View.VISIBLE);
                ContentInicioSesion.setVisibility(View.GONE);
                ContentRegistro.setVisibility(View.GONE);
                TituloInicio.setText("");
            }
        });

        VerContrasena.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ver){
                    ver = true;
                    Contrasena.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    VerContrasena.setImageResource(R.drawable.icono_invisible);
                } else {
                    ver = false;
                    Contrasena.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    VerContrasena.setImageResource(R.drawable.icono_visible);
                }
            }
        });

        IniciarSesion.setOnStateChangeListener(new OnStateChangeListener() {
            @Override
            public void onStateChange(boolean active) {
                if(active){
                    if(!Usuario.getText().toString().equals("") && !Contrasena.getText().toString().equals("")){

                        /* mProcessDialog.setTitle("Por favor Espere");
                        mProcessDialog.setMessage("Iniciando Sesión...");
                        mProcessDialog.setCancelable(false);
                        mProcessDialog.show(); */

                        if(Recordar.isChecked()){
                            save = "SI";
                            dtoUser = Usuario.getText().toString();
                            dtoPass = Contrasena.getText().toString();
                        } else {
                            save = "NO";
                            dtoUser = "";
                            dtoPass = "";
                        }
                        //new IncoCtaAsync().execute();
                    } /* else {
                        mostrarDialogo();
                    } */
                } else {
                    Toast.makeText(getApplicationContext(), "Deslice a la derecha para continuar", Toast.LENGTH_SHORT).show();
                }
            }
        });

        getCredentialsIfExist();

    }

    public void OtorgarPermisos(){

        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS)
                .withListener(new BaseMultiplePermissionsListener(){
                    @SuppressLint("NewApi")
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        super.onPermissionsChecked(report);

                        if (!report.areAllPermissionsGranted()) {

                            alertPermisos.setContentView(R.layout.modal_permisos);
                            alertPermisos.setCancelable(false);

                            TextView titulo = alertPermisos.findViewById(R.id.TextoTitulo);
                            titulo.setText(R.string.titulo_ubicacion);

                            TextView texto = alertPermisos.findViewById(R.id.TextoPermiso);
                            texto.setText(R.string.permiso_ubicacion);

                            ImageButton btnCerrar = alertPermisos.findViewById(R.id.btnCerrar);
                            Button btnPermintir = alertPermisos.findViewById(R.id.btnPermitir);

                            btnCerrar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertPermisos.dismiss();
                                    SesionRegistro.this.finish();
                                }
                            });

                            btnPermintir.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertPermisos.dismiss();
                                    OtorgarPermisos();
                                }
                            });

                            Objects.requireNonNull(alertPermisos.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            alertPermisos.show();

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        super.onPermissionRationaleShouldBeShown(permissions, token);
                    }
                })
                .check();

    }

    private void getCredentialsIfExist() {
        String uso = getDataPreferences("uso");
        String pas = getDataPreferences("pas");
        if(!TextUtils.isEmpty(uso) && !TextUtils.isEmpty(pas)){
            Usuario.setText(uso);
            Contrasena.setText(pas);
            Recordar.setChecked(true);
        }
    }

    private String getDataPreferences(String dato){
        return datosUsuario.getString(dato, "");
    }

    private void DtsCtaUsr(String usuario, String pass){
        SharedPreferences.Editor edit = datosUsuario.edit();
        edit.putString("uso", usuario);
        edit.putString("pas", pass);
        edit.apply();
    }

    private void IncoCtaUso(String cod, String nbe, String apd, String uso, String irl, String imp, String emp, String ruc){
        SharedPreferences.Editor editor = Preferences.edit();
        editor.putString("cod", cod);
        editor.putString("nbe", nbe);
        editor.putString("apd", apd);
        editor.putString("uso", uso);
        editor.putString("irl", irl);
        editor.putString("emp", imp);
        editor.putString("neg", emp);
        editor.putString("ruc", ruc);
        editor.apply();
    }

    private void GuiaUser(){
        SharedPreferences.Editor editor = guiaUsuarios.edit();
        editor.putString("see", "SI");
        editor.apply();
    }

}
