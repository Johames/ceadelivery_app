package com.ceatecsoft.ceadelivery.FragmentsClientes.explorar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.ceatecsoft.ceadelivery.R;

import java.util.Objects;

public class FragmentTiendas extends Fragment {

    public FragmentTiendas(){};

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_clientes_tiendas, container, false);

        ImageView atras = root.findViewById(R.id.Regresar);
        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentExplorar explorar = new FragmentExplorar();
                requireActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.nav_host_fragment, explorar)
                        .commit();
            }
        });

        return root;
    }
}