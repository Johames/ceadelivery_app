package com.ceatecsoft.ceadelivery.ModelsClientes;

public class ModeloPromociones {

    private String tienda, titulo, descripcion, extras, fecha_ini, fecha_fin, codigo, pocentaje, texto, tipo;

    public ModeloPromociones(String tienda, String titulo, String descripcion, String extras, String fecha_ini, String fecha_fin, String codigo, String pocentaje, String texto, String tipo) {
        this.tienda = tienda;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.extras = extras;
        this.fecha_ini = fecha_ini;
        this.fecha_fin = fecha_fin;
        this.codigo = codigo;
        this.pocentaje = pocentaje;
        this.texto = texto;
        this.tipo = tipo;
    }

    public String getTienda() {
        return tienda;
    }

    public void setTienda(String tienda) {
        this.tienda = tienda;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getFecha_ini() {
        return fecha_ini;
    }

    public void setFecha_ini(String fecha_ini) {
        this.fecha_ini = fecha_ini;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getPocentaje() {
        return pocentaje;
    }

    public void setPocentaje(String pocentaje) {
        this.pocentaje = pocentaje;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}