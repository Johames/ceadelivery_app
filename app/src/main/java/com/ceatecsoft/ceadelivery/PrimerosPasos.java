package com.ceatecsoft.ceadelivery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.Timer;
import java.util.TimerTask;

public class PrimerosPasos extends AppCompatActivity {

    int pri = 0;

    Button omitir;
    private ViewPager viewPager;

    private String[] about_title_array = {
            "Ready to Travel.",
            "Pick the Ticket.",
            "Flight to Destination.",
            "Arrive to Destiny",
            "Go to Hotel",
            "Enjoy Holiday"
    };

    private String[] about_Description_array = {
            "Lorem ipsum dolor sit amet consectetur adipiscing elit vulputate sociis aenean morbi egestas felis inceptos litora, metus blandit habitasse donec sociosqu nullam augue fringilla posuere natoque facilisis phasellus sapien. ",
            "Cubilia ultricies placerat facilisi ad dignissim venenatis bibendum, mi suscipit consequat litora vehicula conubia duis torquent, etiam aliquet blandit in maecenas ac. ",
            "Urna egestas potenti torquent integer feugiat magnis turpis senectus, lacinia accumsan quisque sociis gravida aptent posuere porta, interdum cursus cubilia sem mattis mauris pretium. ",
            "Pharetra tristique leo sociis facilisis aptent curabitur vel iaculis, cursus malesuada quis porta nascetur phasellus at, consequat auctor et suscipit per vivamus habitasse. ",
            "Justo habitasse facilisis nisi aenean quisque metus purus habitant bibendum natoque pellentesque, vulputate porttitor mollis vitae massa sollicitudin commodo a primis malesuada eu, risus congue libero molestie mus sagittis condimentum mauris velit ac. ",
            "Sem porta molestie quis gravida habitant felis ante nibh, nullam sapien id vehicula himenaeos tempus hendrerit iaculis erat, convallis diam integer primis facilisi posuere ridiculus. "
    };

    private int[] about_images_array = {
            R.drawable.icono_carrito,
            R.drawable.icono_configuracion,
            R.drawable.icono_deseos,
            R.drawable.icono_perfil_selected,
            R.drawable.icono_historial_selected,
            R.drawable.icono_pedido_selected
    };

    private int[] bg_images_array = {
            R.drawable.wizard_img_01,
            R.drawable.wizard_img_02,
            R.drawable.wizard_img_01,
            R.drawable.wizard_img_03,
            R.drawable.wizard_img_02,
            R.drawable.wizard_img_04
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primeros_pasos);

        setTitle(R.string.title_activity_primeros_pasos);

        omitir = findViewById(R.id.Omitir);
        omitir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                omitir.setEnabled(false);

                Intent sesion = new Intent(getApplicationContext(), SesionRegistro.class);
                startActivity(sesion);

            }
        });

        viewPager = findViewById(R.id.view_pager);

        bottomProgressDots(0);

        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.setPadding(30, 10, 30, 0);
        viewPager.addOnPageChangeListener(viewOnPageChangeListener);

    }

    private void bottomProgressDots(int current_index){
        LinearLayout dotsLayout = findViewById(R.id.layoutDots);
        ImageView[] dots = new ImageView[about_title_array.length];

        dotsLayout.removeAllViews();
        for(int i = 0; i < dots.length; i++){
            dots[i] = new ImageView(this);
            int width_height = 25;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.fondo_dot_default);
            dotsLayout.addView(dots[i]);
        }

        dots[current_index].setImageResource(R.drawable.fondo_dot_selected);
    }

    ViewPager.OnPageChangeListener viewOnPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            bottomProgressDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private class MyViewPagerAdapter extends PagerAdapter {

        private CardView btnNext;
        TextView texto;

        MyViewPagerAdapter(){}

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            assert layoutInflater != null;
            final View view = layoutInflater.inflate(R.layout.item_card_wizard_bg, container, false);
            ((TextView) view.findViewById(R.id.titulo)).setText(about_title_array[position]);
            ((TextView) view.findViewById(R.id.descripcion)).setText(about_Description_array[position]);
            ((ImageView) view.findViewById(R.id.img_about)).setImageResource(about_images_array[position]);
            ((ImageView) view.findViewById(R.id.img_bg)).setImageResource(bg_images_array[position]);

            btnNext = view.findViewById(R.id.btn_next);
            texto = view.findViewById(R.id.Texto);

            if(position == about_title_array.length -1 ){
                texto.setText(R.string.item);
            }

            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int current = viewPager.getCurrentItem() + 1;
                    if(current < about_title_array.length){
                        viewPager.setCurrentItem(current);
                    } else {
                        btnNext.setEnabled(false);

                        Intent sesion = new Intent(getApplicationContext(), SesionRegistro.class);
                        startActivity(sesion);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);

                    }
                }
            });

            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return about_title_array.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            View view   = (View) object;
            container.removeView(view);
        }
    }

    @Override
    public void onBackPressed() {
        pri = pri + 1;
        if(pri == 2){
            PrimerosPasos.this.finish();
        } else {
            Toast.makeText(getApplicationContext(), R.string.salir, Toast.LENGTH_SHORT).show();
        }
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                pri = 0;
            }
        }, 2000);
    }

}
