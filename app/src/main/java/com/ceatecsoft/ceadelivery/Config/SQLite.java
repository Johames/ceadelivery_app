package com.ceatecsoft.ceadelivery.Config;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLite extends SQLiteOpenHelper {

    private static String NAME_DATABASE = "busquedas";
    private static int VERSION = 1;

    private String historial_busqueda = "CREATE TABLE historial(id_historial INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT, cant INTEGER) ";

    public SQLite(Context c) {
        super(c, NAME_DATABASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(historial_busqueda);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS historial");

        this.onCreate(db);
    }

}
