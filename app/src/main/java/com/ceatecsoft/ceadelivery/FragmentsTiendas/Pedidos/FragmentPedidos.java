package com.ceatecsoft.ceadelivery.FragmentsTiendas.Pedidos;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ceatecsoft.ceadelivery.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

public class FragmentPedidos extends Fragment {

    private Dialog dialogFiltroPedidos;

    private TextView TextoBuscar;

    private ImageView Buscar;

    private SwipeRefreshLayout RefreshPedidos;

    private ListView ListaPedidos;

    @SuppressLint({"UseRequireInsteadOfGet", "NewApi"})
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tiendas_pedidos, container, false);

        setHasOptionsMenu(true);

        dialogFiltroPedidos = new Dialog(Objects.requireNonNull(getActivity()));

        TextoBuscar = root.findViewById(R.id.TextoBuscar);

        Buscar = root.findViewById(R.id.Buscar);
        Buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ListaPedidos = root.findViewById(R.id.ListaPedidos);
        ListaPedidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        RefreshPedidos = root.findViewById(R.id.RefreshPedidos);
        RefreshPedidos.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light);

        RefreshPedidos.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //
                    }
                }, 1000);
            }
        });

        FloatingActionButton filtroPedidos = root.findViewById(R.id.FiltroPedidos);
        filtroPedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FiltroPedidos();
            }
        });

        return root;
    }

    @SuppressLint("NewApi")
    private void FiltroPedidos(){
        dialogFiltroPedidos.setContentView(R.layout.modal_filtros_pedidos);
        dialogFiltroPedidos.setCancelable(false);


        Button btnCerrar = dialogFiltroPedidos.findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogFiltroPedidos.dismiss();
            }
        });

        Button btnAplicar = dialogFiltroPedidos.findViewById(R.id.btnAplicar);
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        Objects.requireNonNull(dialogFiltroPedidos.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogFiltroPedidos.show();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem home = menu.findItem(R.id.item_home);
        home.setVisible(false);
        MenuItem promo = menu.findItem(R.id.item_promociones);
        promo.setVisible(false);
        MenuItem pedidos = menu.findItem(R.id.item_pedidos);
        pedidos.setEnabled(false);
        MenuItem prod = menu.findItem(R.id.item_productos);
        prod.setVisible(false);
        MenuItem ventas = menu.findItem(R.id.item_ventas);
        ventas.setVisible(false);
        MenuItem config = menu.findItem(R.id.item_configuracion);
        config.setVisible(false);
        MenuItem valor = menu.findItem(R.id.item_valoracion);
        valor.setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }





}