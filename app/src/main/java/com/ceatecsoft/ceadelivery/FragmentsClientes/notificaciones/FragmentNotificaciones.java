package com.ceatecsoft.ceadelivery.FragmentsClientes.notificaciones;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.ceatecsoft.ceadelivery.R;

public class FragmentNotificaciones extends Fragment {

    public FragmentNotificaciones(){};

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_clientes_notificaciones, container, false);


        return root;
    }
}