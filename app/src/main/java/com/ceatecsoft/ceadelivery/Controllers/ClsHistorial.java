package com.ceatecsoft.ceadelivery.Controllers;

import android.content.Context;
import android.database.Cursor;

import com.ceatecsoft.ceadelivery.Config.SQLite;

public class ClsHistorial {

    SQLite cx;

    public ClsHistorial(Context context) {
        cx = new SQLite(context);
    }

    public void insertHistorial(String texto, Integer cant) {
        cx.getWritableDatabase().execSQL("INSERT INTO mensajes(texto, cant) VALUES ('"+ texto + "', "+ cant + ")");
    }

    public Cursor selectHistorial(){
        return cx.getReadableDatabase().rawQuery("SELECT id_historial, texto, cant FROM historial ",null);
    }

}
