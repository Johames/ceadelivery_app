package com.ceatecsoft.ceadelivery.FragmentsClientes.explorar;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.ceatecsoft.ceadelivery.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

public class FragmentExplorar extends Fragment {

    public FragmentExplorar(){};

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_clientes_explorar, container, false);

        FloatingActionButton cambiar = root.findViewById(R.id.CambiarFragment);
        cambiar.setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseRequireInsteadOfGet", "NewApi"})
            @Override
            public void onClick(View v) {
                FragmentTiendas tiendas = new FragmentTiendas();
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.nav_host_fragment, tiendas)
                        .commit();
                Toast.makeText(getActivity().getApplicationContext(), "Pes aqui va", Toast.LENGTH_SHORT).show();
            }
        });

        return root;
    }
}