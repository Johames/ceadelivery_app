package com.ceatecsoft.ceadelivery.FragmentsTiendas.Valoraciones;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ceatecsoft.ceadelivery.R;

public class FragmentValoraciones extends Fragment {

    private RatingBar Valoracion;

    private TextView CantValoraciones, FiveStar, FourStar, ThreeStar, TwoStar, OneStar;

    SwipeRefreshLayout RefreshResenas;

    private ListView ListaResenas;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tiendas_valoracion, container, false);

        setHasOptionsMenu(true);

        Valoracion = root.findViewById(R.id.Valoracion);
        Valoracion.setEnabled(false);

        CantValoraciones = root.findViewById(R.id.CantValoraciones);
        FiveStar = root.findViewById(R.id.FiveStar);
        FourStar = root.findViewById(R.id.FourStar);
        ThreeStar = root.findViewById(R.id.ThreeStar);
        TwoStar = root.findViewById(R.id.TwoStar);
        OneStar = root.findViewById(R.id.OneStar);

        ListaResenas = root.findViewById(R.id.ListaResenas);

        RefreshResenas = root.findViewById(R.id.RefreshResenas);

        RefreshResenas.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light);

        RefreshResenas.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //
                    }
                }, 1000);
            }
        });

        return root;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem home = menu.findItem(R.id.item_home);
        home.setVisible(false);
        MenuItem promo = menu.findItem(R.id.item_promociones);
        promo.setVisible(false);
        MenuItem prod = menu.findItem(R.id.item_productos);
        prod.setVisible(false);
        MenuItem pedido = menu.findItem(R.id.item_pedidos);
        pedido.setVisible(false);
        MenuItem ventas = menu.findItem(R.id.item_ventas);
        ventas.setVisible(false);
        MenuItem config = menu.findItem(R.id.item_configuracion);
        config.setVisible(false);
        MenuItem valor = menu.findItem(R.id.item_valoracion);
        valor.setEnabled(false);

        super.onPrepareOptionsMenu(menu);
    }

}