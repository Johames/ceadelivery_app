package com.ceatecsoft.ceadelivery.ModelsTiendas;

public class ModeloHistorialBusqueda {

    private String texto;
    private Integer cant;

    public ModeloHistorialBusqueda(String texto, Integer cant) {
        this.texto = texto;
        this.cant = cant;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Integer getCant() {
        return cant;
    }

    public void setCant(Integer cant) {
        this.cant = cant;
    }
}
