package com.ceatecsoft.ceadelivery.FragmentsTiendas.Inicio;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.ceatecsoft.ceadelivery.R;

public class FragmentInicio extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tiendas_inicio, container, false);

        setHasOptionsMenu(true);



        return root;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem home = menu.findItem(R.id.item_home);
        home.setEnabled(false);
        MenuItem promo = menu.findItem(R.id.item_promociones);
        promo.setVisible(false);
        MenuItem prod = menu.findItem(R.id.item_productos);
        prod.setVisible(false);
        MenuItem pedido = menu.findItem(R.id.item_pedidos);
        pedido.setVisible(false);
        MenuItem ventas = menu.findItem(R.id.item_ventas);
        ventas.setVisible(false);
        MenuItem config = menu.findItem(R.id.item_configuracion);
        config.setVisible(false);
        MenuItem valor = menu.findItem(R.id.item_valoracion);
        valor.setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }

}