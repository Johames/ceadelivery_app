package com.ceatecsoft.ceadelivery.FragmentsClientes.perfil;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;

import com.ceatecsoft.ceadelivery.AtencionCliente;
import com.ceatecsoft.ceadelivery.CarritoCompras;
import com.ceatecsoft.ceadelivery.Direcciones;
import com.ceatecsoft.ceadelivery.PreguntasFrecuentes;
import com.ceatecsoft.ceadelivery.PrimerosPasos;
import com.ceatecsoft.ceadelivery.R;
import com.ceatecsoft.ceadelivery.ReclamoSugerencia;
import com.ceatecsoft.ceadelivery.SesionRegistro;

import java.util.Objects;

import static com.ceatecsoft.ceadelivery.PrincipalClientes.DURATION_TRANSACTION;

public class FragmentPerfil extends Fragment {

    private Dialog dialogAcercaDe, dialogCerrarSesion;

    private static final long DURATION_TRANSACTION = 500;

    private ImageView ImagenEmpresa;
    private TextView CantDeseos, CantPedidos, CantResenas;

    public FragmentPerfil(){};

    @SuppressLint({"UseRequireInsteadOfGet", "NewApi"})
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_clientes_perfil, container, false);

        dialogAcercaDe = new Dialog(Objects.requireNonNull(getActivity()));
        dialogCerrarSesion = new Dialog(Objects.requireNonNull(getActivity()));

        ImagenEmpresa = root.findViewById(R.id.ImagenEmpresa);
        CantDeseos = root.findViewById(R.id.CantDeseos);
        CantPedidos = root.findViewById(R.id.CantPedidos);
        CantResenas = root.findViewById(R.id.CantResenas);

        CardView AdministrarDirecciones = root.findViewById(R.id.AdministrarDirecciones);
        AdministrarDirecciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transition intro = new Explode();
                intro.setDuration(DURATION_TRANSACTION);
                intro.setInterpolator(new AccelerateInterpolator());

                requireActivity().getWindow().setExitTransition(intro);

                Intent direcciones = new Intent(getActivity(), Direcciones.class);
                startActivity(direcciones, ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity()).toBundle());
            }
        });

        CardView PreguntasFrecuentes = root.findViewById(R.id.PreguntasFrecuentes);
        PreguntasFrecuentes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transition intro = new Explode();
                intro.setDuration(DURATION_TRANSACTION);
                intro.setInterpolator(new AccelerateInterpolator());

                requireActivity().getWindow().setExitTransition(intro);

                Intent preguntas = new Intent(getActivity(), PreguntasFrecuentes.class);
                startActivity(preguntas, ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity()).toBundle());
            }
        });

        CardView AtencionCliente = root.findViewById(R.id.AtencionCliente);
        AtencionCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transition intro = new Explode();
                intro.setDuration(DURATION_TRANSACTION);
                intro.setInterpolator(new AccelerateInterpolator());

                requireActivity().getWindow().setExitTransition(intro);

                Intent atencion = new Intent(getActivity(), com.ceatecsoft.ceadelivery.AtencionCliente.class);
                startActivity(atencion, ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity()).toBundle());
            }
        });

        CardView ReclamosSugerencias = root.findViewById(R.id.ReclamosSugerencias);
        ReclamosSugerencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transition intro = new Explode();
                intro.setDuration(DURATION_TRANSACTION);
                intro.setInterpolator(new AccelerateInterpolator());

                requireActivity().getWindow().setExitTransition(intro);

                Intent reclamos = new Intent(getActivity(), ReclamoSugerencia.class);
                startActivity(reclamos, ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity()).toBundle());
            }
        });

        CardView PrimerosPasos = root.findViewById(R.id.PrimerosPasos);
        PrimerosPasos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transition intro = new Explode();
                intro.setDuration(DURATION_TRANSACTION);
                intro.setInterpolator(new AccelerateInterpolator());

                requireActivity().getWindow().setExitTransition(intro);

                Intent primeros = new Intent(getActivity(), PrimerosPasos.class);
                startActivity(primeros, ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity()).toBundle());
            }
        });

        CardView AcercaDe = root.findViewById(R.id.AcercaDe);
        AcercaDe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MostrarDialogAcercaDe();
            }
        });

        CardView IniciarSesion = root.findViewById(R.id.IniciarSesion);
        IniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transition intro = new Explode();
                intro.setDuration(DURATION_TRANSACTION);
                intro.setInterpolator(new AccelerateInterpolator());

                requireActivity().getWindow().setExitTransition(intro);

                Intent registro = new Intent(getActivity(), SesionRegistro.class);
                startActivity(registro, ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity()).toBundle());
                Objects.requireNonNull(getActivity()).finish();
            }
        });

        CardView CerrarSesion = root.findViewById(R.id.CerrarSesion);
        CerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MostrarDialogCerrarSesion();
            }
        });

        return root;
    }

    @SuppressLint("NewApi")
    private void MostrarDialogAcercaDe() {
        dialogAcercaDe.setContentView(R.layout.modal_acerca_app);
        dialogAcercaDe.setCancelable(false);


        Button btnCerrar = dialogAcercaDe.findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAcercaDe.dismiss();
            }
        });

        Button btnAplicar = dialogAcercaDe.findViewById(R.id.btnAplicar);
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        Objects.requireNonNull(dialogAcercaDe.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAcercaDe.show();
    }

    @SuppressLint("NewApi")
    private void MostrarDialogCerrarSesion() {
        dialogCerrarSesion.setContentView(R.layout.modal_cerrar_session);
        dialogCerrarSesion.setCancelable(false);


        ImageButton btnCerrar = dialogCerrarSesion.findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCerrarSesion.dismiss();
            }
        });

        Button btnCerrarSesion = dialogCerrarSesion.findViewById(R.id.btnCerrarSesion);
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        Objects.requireNonNull(dialogCerrarSesion.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCerrarSesion.show();
    }

}