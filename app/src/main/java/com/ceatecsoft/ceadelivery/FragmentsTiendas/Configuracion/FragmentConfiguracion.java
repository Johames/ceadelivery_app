package com.ceatecsoft.ceadelivery.FragmentsTiendas.Configuracion;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.ceatecsoft.ceadelivery.R;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class FragmentConfiguracion extends Fragment {

    private Dialog dialogEditarDireccion, dialogHorario, dialogAgregarEditarHorario, dialogAgregarEditarUsuarios;

    private ImageView ImagenEmpresa;

    private TextView NombreEmpresa, DescripcionEmpresa, DireccionEmpresa;

    private MapView DireccionMap;
    private GoogleMap googleMap, Mapa;

    int count = 0;

    private String origen = "horario", opcion = "agregar", turno = "1", ubicar = "";

    private double lat = -31;
    private double lng = 151;

    private String nombEmp = "Nombre de la Empresa", descEmp = "Descripción de la Empresa";

    private LinearLayout Content1Turno, Content2Turno;

    @SuppressLint({"UseRequireInsteadOfGet", "NewApi"})
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tiendas_configuracion, container, false);

        setHasOptionsMenu(true);

        dialogEditarDireccion = new Dialog(Objects.requireNonNull(getActivity()));
        dialogHorario = new Dialog(Objects.requireNonNull(getActivity()));
        dialogAgregarEditarHorario = new Dialog(Objects.requireNonNull(getActivity()));
        dialogAgregarEditarUsuarios = new Dialog(Objects.requireNonNull(getActivity()));

        ImagenEmpresa = root.findViewById(R.id.ImagenEmpresa);
        ImageView editarDireccion = root.findViewById(R.id.EditarDireccion);
        NombreEmpresa = root.findViewById(R.id.NombreEmpresa);
        DescripcionEmpresa = root.findViewById(R.id.DescripcionEmpresa);
        DireccionEmpresa = root.findViewById(R.id.DireccionEmpresa);
        DireccionMap = root.findViewById(R.id.DireccionMap);
        CardView horarioAtencion = root.findViewById(R.id.HorarioAtencion);
        CardView listaUsuarios = root.findViewById(R.id.ListaUsuarios);

        DireccionMap.onCreate(savedInstanceState);
        DireccionMap.onResume();

        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e){
            e.printStackTrace();
        }

        DireccionMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap mMap) {
                googleMap = mMap;

                mMap.setMinZoomPreference(12);
                UiSettings uiSettings = mMap.getUiSettings();
                uiSettings.setScrollGesturesEnabled(false);
                uiSettings.setTiltGesturesEnabled(false);
                uiSettings.setRotateGesturesEnabled(false);
                uiSettings.setZoomGesturesEnabled(false);

                if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                }

                final LatLng coordenadas = new LatLng(lat, lng);
                CameraUpdate miUbicacion = CameraUpdateFactory.newLatLngZoom(coordenadas, 14);
                mMap.animateCamera(miUbicacion);

                mMap.addMarker(new MarkerOptions()
                        .position(coordenadas)
                        .title(nombEmp)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                        .snippet(descEmp));

            }
        });

        final TapTargetSequence sequence = new TapTargetSequence(Objects.requireNonNull(getActivity()))
                .targets(
                        TapTarget.forView(editarDireccion, "Modificar la Dirección", "Al tocar sobre este botón se abrirá una ventana en la cual se podrá modificar la direccion asi como la referencia de la misma.")
                                .cancelable(false)
                                .transparentTarget(true)
                                .targetRadius(20)
                                .outerCircleColor(R.color.colorPrimary)
                                .targetCircleColor(R.color.colorWhite)
                                .titleTextSize(20)
                                .titleTextColor(android.R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.colorWhite)
                                .textColor(android.R.color.white)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.colorPrimary)
                                .drawShadow(true)
                                .tintTarget(true),
                        TapTarget.forView(horarioAtencion, "Horario Atención", "Al tocar sobre este botón se abrirá una venta en la que podrás ver tu horario de atención, modificarlo o agregar un nuevo horario.")
                                .cancelable(false)
                                .transparentTarget(true)
                                .targetRadius(20)
                                .outerCircleColor(R.color.colorPrimary)
                                .targetCircleColor(R.color.colorWhite)
                                .titleTextSize(20)
                                .titleTextColor(android.R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.colorWhite)
                                .textColor(android.R.color.white)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.colorPrimary)
                                .drawShadow(true)
                                .tintTarget(true),
                        TapTarget.forView(listaUsuarios, "Lista de Usuarios", "Al tocar sobre este botón se abrirá una venta en la que podrás ver la lista de tus usuarios, modificarlos o agregar uno nuevo.")
                                .cancelable(false)
                                .transparentTarget(true)
                                .targetRadius(20)
                                .outerCircleColor(R.color.colorPrimary)
                                .targetCircleColor(R.color.colorWhite)
                                .titleTextSize(20)
                                .titleTextColor(android.R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.colorWhite)
                                .textColor(android.R.color.white)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.colorPrimary)
                                .drawShadow(true)
                                .tintTarget(true)

                )
                .listener(new TapTargetSequence.Listener() {
                              @Override
                              public void onSequenceFinish() {
                                  new Handler().postDelayed(new Runnable() {
                                      @Override
                                      public void run() {
                                          //ModalMostrarInfoAgain();
                                      }
                                  }, 500);
                              }

                              @Override
                              public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                              }

                              @Override
                              public void onSequenceCanceled(TapTarget lastTarget) {
                                  Toast.makeText(getActivity(), "Se ha cancelado la guía.", Toast.LENGTH_SHORT).show();
                              }
                          }
                );

        sequence.start();

        Picasso.get()
                .load("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADhCAMAAADmr0l2AAAAilBMVEUAAAD////m5ubl5eXk5OT29vbq6ur7+/vw8PDy8vLt7e34+PhGRka+vr7Y2Nhubm62trYXFxfQ0NBfX1+dnZ3e3t7GxsaQkJCwsLBVVVUhISHKyspoaGg+Pj4nJydiYmIyMjJ2dnZNTU2np6eFhYU3NzeYmJgQEBAkJCSHh4caGhpJSUl1dXWSkpLxztLCAAAUWUlEQVR4nNVd6WKrKhAWERSIJjExS5O2Zml709O+/+td9wiCgkubzq9Tjk7mk21mmBksUBKCdk6OWza5TtEEUfVY1eRVTbhosnHV5EGIKULMCQ+76+tqNavTarWan07b9WXhH8LAoQwRZsy/FLZLfmsagAi5JLh87WOri5bx+XhdeIb8fxug42/PndB4nKfFgT06QJgJQJztubvjJPQxO+48l1AbPihAiBHwdtfPPuAq2q9D18UJxkcDCLHH7MX+aRC6nN62AUH0oQBCmwJ38fUxArqc9msM7gvrrwOkhAan0cAV9O9Q/eZYAB2YU/2BsqkGsGy6AwS7/djwUtqvqxFSUA1gJUaX/BYqiXglVU2oaiKqJgCi9RTocroG6ZbqNSWTCKuQ34LNHsHFt3A6vpgDMXPmL9Phs6zP14Bh0xHFyW/ZzTmlOeYdj4WvU6LL6TWsfrKP4tEfoIt+Al5Kx+hXAEZzfRFf9ol6fVtv1+vt7fb1ep7FZhBv9o8DdK46gj3tj9dLsm0jxu7bGmAsXROIE27W1/lqpqUcfJOfBbjT2NRn603AkkWQpty5RQCm3DClmZhesNnNurk9+z8I8PBfhzTL1TYoBYBK/rV9DQTredzBdO8Au2Q2KUDWNTpXu8jVFiAnkljH9uHUPlo/1y6BxgCdkur7SEH1fbBq2rT33XWXDMu6ANr8YbLhhd+tozUOWf6yPn/LLQnhkljZxKomVLQAt21r+Jj5Kc/kfVK9acKfJR1Pw3mLwfVxS2aia8K/rouWo7CpbFdfzI9bPu97VDGTKgYa/JMeocC5vKl/ZeWY8TeyJtx35e8uV36i+LUC1LVWICbEV3s8PndG/E0AEvXW/hW4qL4qDgGY/GkjEB6VP3Zl0wCMVGvc0xWz1MvQy16TAUwhYoBPS8UPvkX3wT0ewI1q7t8ihvN3xwOYL/uBash8bsq9YCyADFwUv7VlgJZvjg0QAFvlKfguPuo4AJPJpdjc96ktM8il0A4wGTiKjfEr/65GLguVAJAqlpdUPwSZ06n8Dm0ANbcJASAAF/ncP+P0R0y2iftGWTYVGzFFK+lPvBcy0PJFegdYNtka/NONvnrMLptqmol8nD5HngZ/HVXNlfqUzmH5YO2LldRHVcup1iP3po10548DDf4ayrYnw/exrYbhOKdRHXNW2okvhzGsCVc2y5dB65waHyAIpcM0HA6QyvAlusTY54ldAAGRdeKH7w4F+Cxhu5YJMDVAAKQO2JDBIQCpBN9boBBgaoAglPlgiz7sAxBSIFHqz0QpwNQAAZZ872WQvdwDYLK/S9bPk6sWYGqALpa5Sz4zhEqAMtd9cdSBJNb77S5A78OaAYcpmN2aMj1jquZfO3y5U94AJPrZAohPcW+2NbG7nCzzjEp+spMZIWAhmTVAziz9d8vxmeRbbXr3CLODzeU6P+73x/m7H0XZiUq/Hj80fbJH9YhSWxMS+2gD+s0pdLm9Cgvg23zruwlsCM3nrMSvtzU3l/wGk2WgKUAdoAeii9JN/LyLEOmxKAVNU39hCjBqjoONtgAFQGh7bnhsPT98OobMS7rRcNVt9uEyco0A0qYsgYEAKUBoE+K3+P9KevMJMd5WggabD4INALKmS2tjJIAHocc2mtFOZ/+OT5M/2DRG6YkZANw2hKiOWTUB2ixS+/0adAyAGX/KmqP0G+gClGw1O0MBEDMMTVgzI/6e4+4aPMJ8aVP7ZAqipLEurKunNC10ahxYsscm/D3HYd8ii7fiZIbzMFS6qH1n2PDA3AApn4LNHod3hkUT81Uu2zZaMF3+xbbS0EROkh6XKNuNATozcZXbELsqH2oHXVwMTdyMrGGL5ysF5+VrArTFt56ZiZ8z0dF7h3WdEDbyozJxF1oijR5siJeucNoAk/4ziL4Qae7iDv4cwKajZtsNsDFAswVUF2DSfwPwJQhRO38BYNOLcegEKL6RG4C6ADEYGBz0agawcaQQdwEU194XYASwuXab0tYMIBB15l07wEhY358CI4DuCJGHuxb+EoDiNHxxFC6LHKA4gS6gBKjjUgCH4fgqrV7TJYJEtfIkuCxoSYSxxuc4ApYTqR5jJaGqCRUtwBkjZNt6c4Ccv6yJeeCf+IGqx7zk/zlVDQn68TKgjo5LoWhjI0UfHoGcvyJ+tdkr9R6vAySiFX9hBqFT0FS/VtOOGKUViLEfhztA3ppwhc7elxaWFkDqjIXP+sDYBCCJBbkVAD1xjw+AZNVSAYStMVCGdHKhiVNLtA1L41wAiATd9WSUGELaY9jM6CM0SwwR7J9/UoDiGv+JjVzx0mOo3rQHjgnASHg9lAEUD6rXJmcNEEkczkNoQ4zOMoR1Zi8DKK62rhFA2TnNEJozI4BIeD2UAPziH1kAgzwFhzb9xAPpntmjlcchdOGqAJjsg6VqIHhCZ16hCehpMmD05J450NNkCp3FEfxIFJWaTPElRDNgh/jDlA5dVOIoHkoM1/iDqkfkI8oRIz2vpfylNSGenbrtYx4I2v4E+UsXUuNfAlSuCdjj337Gdt1cgqKWdsnE1waoCIUaREcjgNAVrIoFg3WAgN/kn2nHqiUAFPehUSjCBgBt7MQ8BFAHKOqR29wDqQ2w6WUegS7FL2jGAIjesoje5yBEwhJDsVkPGuaU69EeGAEkwkL3TWqLjMfbEXNkGME7SQ7hE8MmAD3AW7P/aj1IBS3mkE9B3SDzjlyR3uRTM4CCGCEtAULB+fZcfDruOKwa6g2AcJopmEzC/FeVZxMZlU3p2Qrv6L5mXuRcVeN9KZeKoV78J2seJ45C7wV/3fhSwaXwkgmbART6toqi1VS2JefBo9CpBFh2UseUwcJecAClss33wNw0NAtpnMT3oRkyAwgRbzCs0/FrNQWsRqguQEFLGo2eiBlAW3C6nEkBUDgwu8d561ZCmAjghylAjHhHvodzgPwIPQPj6MGJAFrUEKCozaRuzxQgb8vtiCnASTTRXgARbzPs3awHI14PiaApwIn2eXOAyRNx/f3/En3dEqPSVsg4/nOUI5c2gHy0YQZQEV8qHL/6GFpECFa4AT7esjuek8kj/scAaBKPmv0lxD9sXWI5jB+hB+OIXNoMHBuJHFvXZVGNKN69Fieqmmjrm435lOhkq6ihNZERz8EFluByfzMHiB8KIK/M+MBC/BR8fyCAS0NzKSNembkBi/H+okYYRjdAeypN5sXrAZBf8Y7JHOT8hVnYsilAL54G4Jn0ABjFPAsr4NS3MzIHCKcyl76QJDyyCyB/lLYMLH6Xzs63TQE2zpBHom2fHhQCRUKLdzdcM4CmGZo9owu76EKdZvxnp4eBNx0OFm/m71CPHFs0+tFSTovCrjHKERYOKi8W72+KkrFoXhlPjI8ah5blaNHM8s5TTDx+Gd1a/MaYxuabZ5OBeAqAccXfZE0QduUvi3dKp4Ex5gDHDLC406oXQJtwTM4Wtw3u+wEc+4A+pyqexwggZNy+N7Ni7qO5vQDiSWz6SnZDgFyXvfEAt/16EHoTnA++Vou5GUB+vsQWV0QlO7HqAXB4HGyTvu/8zQByy+anxa3wO08OsLWYRgJQPL0Zgw41/sVP6hQDgYTb+JY8wIXXUZXLdasmUjXRZAcefSdcghr/cqOvmlqqcvFnnUt+iBblUwxVtbTHR1dH312OfzGidA6DOOVTCOk+mI/5YkiP7pcJGMefnzKta0LLnrUMewMcmk4g0sqdAuDnAIAj7/XKnNxBAP+L+gNsZJQMowGVFqYCOGqw0/cDAhTD4YbQU/SAAOVVifrRScZ/BIBFIo9unW0hGpCoGZsSlfHXPQxqAfgREtNK6bWmzsqx2nRi8p/UqpQuyV6+0wb3+GJVeZZo2FUTFX1GeEB5FtTmAvNxjzFfFdhxR+rCa2qV9i2w0wDIKWtZ8FTvCj9YTEHpR28Iy/nrASTCfsUBXAwCCMkoMV27rBTzSAA/eHOpp8F7F2CEuMoVaOGvM0S5iSIAXJOBACWVXgxpGXlwEEDeol9anP7xNbQHh3vxyzjf/gA5u+ZlFK9aXQAxH9OQVmV8ZG+AvFct5v2iT6yHBc17AJxBm+ELtTv4FwDVHgbeL/ps8eH2PRgKH2RYml3gdfHv/uAcw7PFx3E1a0F0DYnGkJaVdtOlNRtcE1gAuBJOlw7DAdpub7PiBoYXPRYi564W77JdjwAQm1fLyelI8AgAxfNBXvn4GgEg9GjcB98sO7sbDJA/4d0IZ/SrEQAmv+X0yHc9254mf7Mzej7KInZGAJjog9j4orcXmqmggwG63DnQSyTEyfQJBJKdZbjYcMM/enlB+cEA+ejXM7KEzLg0s3XQZWqlgSwtfqykPS2LSujyrwAKBjg/5ebAEqzU49DL1Ep/BhMT+troHRjzVzQJWTg3YIkBycAxd1lInVQYXTRNi48dql1T60DH0eEvHVGOEIC+IRZw+V/DtKeyDe17NHs+Z1mktSGuIlbnTwlD/e8AFRzsXnqpDe+wvZA+AKFNGfVXK1IXAFK07qww87RGtF5+k3qrvZ8w6wUQenwOyIyl5Vb4SXjqARBixsJtnHK0OQGg63Toba9O5mG684/SVT3ehrW70gwACg6ZbQaQ123esHFaAWHOJS7fDzkBkoU4mqun4ip0s/PpO//qLDzeVZl8BkNUjLqnaVpByAsQYlOAYf32mY9LQ4BAcfx7PSDxqq9LTUH4OB2MAUJuvr3keROCp+jqGgEM1qLScpUIsDkJ7qjnr7T6VnXzW8G/4VhdGwXoipkvZ5bnLvHT5D+T3CVfqICR0VxW7NEL/NvsJf0Yn8+nXWhL+DNZUb0vH6D8Pj+d3CU+NPda5C4JcUr3+I1WCxoBe6c6MQsBbHgAGM1Ka9dvWuT5S0v1J/RycZgHdTwMWChKAqnjWGnkJz8J312NsI1EuFOsgJdMn7VbRoHUwk4YY1mzJCzEI25jqNcgXp3sxa6wFmG9/CTJS1Y6PvhxticagTfSsVmj1/KiN+m20uAPoo5gsC+/PqRVgUn8HL6mQybNwvYEj3tY5lyrADK/21b42DElwMaiwTSu3Jz5qOsGSSGBwycFQIx5V99Xmc6tALjQc0nsD7oAD3oMzxfEWkvH817nT4+CMo+eHx8xbebRlwAJWeh751fJIg87HbmBfqji8kKIDZUA+VzkvBprBlAMaF0Q6bIMIQULIzPPOiWLfF6wX3YalfI/dExmgWZpAI2idLxgF23IvZYF5lfoPZMBhAT55tUZn5JFPt3IhEUmNacRixbmfvCnBSGyRYYIm2CMa9VImKASB816MpC6Yb+w15fjjjBG7jJ5yZqetuzaC/0rKdFgXQlAIYXq5tYAekKW6jzrwvo2YTNnSPHe+HpZ+P6mIN9fXK7xAHZzpwlQDGMJi2ok+S4pGIVPEZeIkeyy3vsotUPHos+0dHzWjVWiCoy5J2KQy1/WVRPO9fI6GaUqhVk4UTWH/vRWXMlbdaWAYFHIXwK0Y/7/3Rxg1vseMVvpfojmlNSUbcHzEttAqIwnjOB1CRDaIIx/TGgjetvUDmuE2IoTEAEKJsUbzh+AlAw4DpuaTqRcJrAwh2gDoFB4rKxt6OGJsh/HoWO50AszsKxtWAco1jPIHkCT5ciPRZFsBkqrUyLBQtglD0yVGTgiffiSDpTWFxVvWvpHwFTVqMalZDJ5wgz0ZQCRGGm2mCp1dWx6FwWduSVACK172QsxkvRtgpKT09C7oNMu2L2SB3c9n5kp9LA0Q/b9+I+7rWCyyjc/Sz6qeSQ4gO5D73m6dKxyZpr3TUxW+uYnKSRqgLJLI/8azY1uDPl7tIza73z5M1uDitZdt/b88S5cAhGgGI74+NpnK6VKGn9d+11Vy9ee/hd7PQKdXFsouNoAiKcqYPQjhKlYbKQBsFFT/C+R38wakNwgOVUFo+npmF1S0wkQN269+yP0kh8aadwB+keV7k3HHaC18MWJyi5PS1ftW1yl9/A+PO09uwNgPT5zqrrEE5Lt1eQvAUJoKa44+XOG08GVX6HCuSwKwg500B/TurdIEV8quaYW5LeJ/CnTcA5sRQyACqCN0egX8UxHZ4RVQQ5KgJDyFY8fmeJEBTUGmCAM/vttyfXov4CqQ6DVAG3b/SMaje/K5e8ECCWXvj8g+awta7sNYPLAHzidWLst8teuqVXkKTy8cbgoLwdT7YMt186mTY/uZntPQ2xb5KcKXTSnRJdz3EkKM45FC9AhP5RbE9UDyZ/sgU9BL0xyi5COuVQHCN2HnYc+gJ3peN0Akz7cPFQYV0lPG6aRb6gD0HYPD+jvfgnT/WEcgBDRgRUcxqdZnjI6DkDbYY/mxHglmimxegDTEOKHckRty9yEPgBVl6k9kGK6QZW+3CV/43QJKIt1sGiSexTN6Rwx/eooHcp2BTALSqejVU4bQlcqDUrvZ03UAaYx3L+v1Tzt0jT4aQBm+8Uve2r2HinXhCkA/non7piQEjs2wHRZtn/NoTi3i8uHpwU4erlbXfIBbc8+Gw0gcCeojN5F326vKmFSl0XZ1lJaIfzhxSZNZDOslllEGxrcnFZvYsD/wW3/3wYwpHvZHN/U5bJQfjGbsUWPuj996HmRZg22j6gBLouWMY92P2AnxguCbL00c5n8gwACwAwTJo3p7N+Tmn8DYPLaZsKYjP0hlWVQCabBABMKvibx2bzdCul/HSAi0WWk+sV3incVnF8HmNaTAeFtxDX1eRuAO/0+wKwiEKKb11Gqwb+cQkz6VQRqB9i33GVpQUMCgL8dOB1na5+4WMvD0CzWIZPfqopd1O4yM7lMDVeXwaXFNCjzwtu/nhMyXq1tkjGS8te+bI6TX6psaxTrUA2JtHoVChZb49J/b2sfMkZHqC/Ky9/PmmgVANrZpzt8z1cznb6MV/PvA2KEpnVxRgFYl38CgDUBaBQeFpfv7em4Wp1XsxqdV6uv03bnH8IgZVkV/Rkd4P+O3JG80hdCTQAAAABJRU5ErkJggg==")
                .placeholder(R.drawable.icono_cerrar)
                .error(R.drawable.icono_cancel)
                .into(ImagenEmpresa);

        editarDireccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditarDireccion();
            }
        });

        horarioAtencion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                origen = "horario";
                MostrarHorarioUsuarios();
            }
        });

        listaUsuarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                origen = "usuario";
                MostrarHorarioUsuarios();
            }
        });

        return root;
    }

    @SuppressLint({"NewApi", "UseRequireInsteadOfGet"})
    private void EditarDireccion(){
        dialogEditarDireccion.setContentView(R.layout.modal_editar_direccion);
        dialogEditarDireccion.setCancelable(false);

        count = 0;

        EditText Direccion = dialogEditarDireccion.findViewById(R.id.Direccion);
        MapView DireccionSeleccionar = dialogEditarDireccion.findViewById(R.id.DireccionSeleccionar);
        final TextView Latitud = dialogEditarDireccion.findViewById(R.id.Latitud);
        final TextView Longitud = dialogEditarDireccion.findViewById(R.id.Longitud);
        Button btnCerrar = dialogEditarDireccion.findViewById(R.id.btnCerrar);
        Button btnAplicar = dialogEditarDireccion.findViewById(R.id.btnAplicar);


        DireccionSeleccionar.onCreate(dialogEditarDireccion.onSaveInstanceState());
        DireccionSeleccionar.onResume();
        MapsInitializer.initialize(Objects.requireNonNull(getActivity()));
        DireccionSeleccionar.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap mMap) {
                Mapa = mMap;

                mMap.setMinZoomPreference(12);
                mMap.setIndoorEnabled(true);
                UiSettings uiSettings = mMap.getUiSettings();
                uiSettings.setIndoorLevelPickerEnabled(true);
                uiSettings.setMyLocationButtonEnabled(true);
                uiSettings.setMapToolbarEnabled(true);
                uiSettings.setCompassEnabled(true);
                uiSettings.setZoomControlsEnabled(true);
                uiSettings.setScrollGesturesEnabled(true);

                if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                }

                mMap.setMyLocationEnabled(true);

                mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {
                        if(count == 0){
                            final LatLng coordenadas = new LatLng(location.getLatitude(), location.getLongitude());
                            CameraUpdate miUbicacion = CameraUpdateFactory.newLatLngZoom(coordenadas, 14);
                            mMap.animateCamera(miUbicacion);
                            count++;
                        }

                    }
                });

                /* final LatLng coordenadas = new LatLng(lat, lng);
                CameraUpdate miUbicacion = CameraUpdateFactory.newLatLngZoom(coordenadas, 14);
                mMap.animateCamera(miUbicacion); */

                mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {
                        mMap.clear();
                        mMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title(nombEmp)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                                .snippet(descEmp));
                        ubicar = latLng.toString();

                        lat = Double.parseDouble(ubicar.substring(10, ubicar.indexOf(",")));
                        lng = Double.parseDouble(ubicar.substring(ubicar.indexOf(",") + 1, ubicar.length() - 1));

                        Latitud.setText(String.valueOf(lat));
                        Longitud.setText(String.valueOf(lng));
                    }
                });

            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogEditarDireccion.dismiss();
            }
        });

        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        Objects.requireNonNull(dialogEditarDireccion.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogEditarDireccion.show();
    }

    @SuppressLint("NewApi")
    private void MostrarHorarioUsuarios(){
        dialogHorario.setContentView(R.layout.modal_ver_horarios_usuarios);
        dialogHorario.setCancelable(false);

        TextView TituloModal = dialogHorario.findViewById(R.id.TituloModal);
        if(origen.equals("horario")){
            TituloModal.setText(R.string.ver_horario);
        } else if(origen.equals("usuario")){
            TituloModal.setText(R.string.ver_usuario);
        }

        ImageView CerrarModal = dialogHorario.findViewById(R.id.CerrarModal);
        CerrarModal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogHorario.dismiss();
            }
        });

        ListView ListaHorarioUsuarios = dialogHorario.findViewById(R.id.ListaHorarioUsuarios);
        ListaHorarioUsuarios.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });

        FloatingActionButton AddHorario = dialogHorario.findViewById(R.id.AddHorario);
        AddHorario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(origen.equals("horario")){
                    opcion = "agregar";
                    AgregarEditarHorario();
                } else if(origen.equals("usuario")){
                    opcion = "agregar";
                    AgregarEditarUsuario();
                }
            }
        });

        Objects.requireNonNull(dialogHorario.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogHorario.show();
    }

    @SuppressLint("NewApi")
    private void AgregarEditarHorario(){
        dialogAgregarEditarHorario.setContentView(R.layout.modal_agregar_editar_horario);
        dialogAgregarEditarHorario.setCancelable(false);

        TextView TituloModal = dialogAgregarEditarHorario.findViewById(R.id.TituloModal);
        if(opcion.equals("agregar")){
            TituloModal.setText(R.string.add_horario);
        } else if(opcion.equals("editar")){
            TituloModal.setText(R.string.mod_horario);
        }

        Content1Turno = dialogAgregarEditarHorario.findViewById(R.id.Content1Turno);
        Content2Turno = dialogAgregarEditarHorario.findViewById(R.id.Content2Turno);

        final Button OneTurn = dialogAgregarEditarHorario.findViewById(R.id.OneTurn);
        final Button TwoTurn = dialogAgregarEditarHorario.findViewById(R.id.TwoTurn);

        OneTurn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turno = "1";

                OneTurn.setBackgroundResource(R.drawable.fondo_border_left_ripple_white);
                OneTurn.setTextColor(getResources().getColor(R.color.colorWhite));

                TwoTurn.setBackgroundResource(R.drawable.fondo_border_right_default_ripple_white);
                TwoTurn.setTextColor(getResources().getColor(R.color.colorPrimary));

                Content1Turno.setVisibility(View.VISIBLE);
                Content2Turno.setVisibility(View.GONE);
            }
        });

        TwoTurn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turno = "2";

                OneTurn.setBackgroundResource(R.drawable.fondo_border_left_default_ripple_white);
                OneTurn.setTextColor(getResources().getColor(R.color.colorPrimary));

                TwoTurn.setBackgroundResource(R.drawable.fondo_border_right_ripple_white);
                TwoTurn.setTextColor(getResources().getColor(R.color.colorWhite));

                Content1Turno.setVisibility(View.GONE);
                Content2Turno.setVisibility(View.VISIBLE);
            }
        });



        Button btnCerrar = dialogAgregarEditarHorario.findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAgregarEditarHorario.dismiss();
            }
        });
        Button btnAplicar = dialogAgregarEditarHorario.findViewById(R.id.btnAplicar);
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(opcion.equals("agregar")){
                    Toast.makeText(getActivity(), "Agregar", Toast.LENGTH_SHORT).show();
                } else if(opcion.equals("editar")){
                    Toast.makeText(getActivity(), "Editar", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Objects.requireNonNull(dialogAgregarEditarHorario.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAgregarEditarHorario.show();
    }

    @SuppressLint("NewApi")
    private void AgregarEditarUsuario(){
        dialogAgregarEditarUsuarios.setContentView(R.layout.modal_agregar_editar_usuarios);
        dialogAgregarEditarUsuarios.setCancelable(false);

        TextView TituloModal = dialogAgregarEditarUsuarios.findViewById(R.id.TituloModal);
        if(opcion.equals("agregar")){
            TituloModal.setText(R.string.add_usuario);
        } else if(opcion.equals("editar")){
            TituloModal.setText(R.string.mod_usuario);
        }





        Button btnCerrar = dialogAgregarEditarUsuarios.findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAgregarEditarUsuarios.dismiss();
            }
        });
        Button btnAplicar = dialogAgregarEditarUsuarios.findViewById(R.id.btnAplicar);
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(opcion.equals("agregar")){
                    Toast.makeText(getActivity(), "Agregar", Toast.LENGTH_SHORT).show();
                } else if(opcion.equals("editar")){
                    Toast.makeText(getActivity(), "Editar", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Objects.requireNonNull(dialogAgregarEditarUsuarios.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAgregarEditarUsuarios.show();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem home = menu.findItem(R.id.item_home);
        home.setVisible(false);
        MenuItem promo = menu.findItem(R.id.item_promociones);
        promo.setVisible(false);
        MenuItem prod = menu.findItem(R.id.item_productos);
        prod.setVisible(false);
        MenuItem pedido = menu.findItem(R.id.item_pedidos);
        pedido.setVisible(false);
        MenuItem ventas = menu.findItem(R.id.item_ventas);
        ventas.setVisible(false);
        MenuItem config = menu.findItem(R.id.item_configuracion);
        config.setEnabled(false);
        MenuItem valor = menu.findItem(R.id.item_valoracion);
        valor.setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }





}