package com.ceatecsoft.ceadelivery.FragmentsTiendas.Productos;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ceatecsoft.ceadelivery.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

public class FragmentProductos extends Fragment {

    private Dialog dialogAgregarProducto, dialogAgregarMarcaCategoriaUnidad;

    private TextView TextoBuscar, Paso1, Paso2, Paso3;

    private ImageView Buscar;

    private SwipeRefreshLayout RefreshProductos;

    private ListView ListaProductos;

    private RelativeLayout ContentPaso1, ContentPaso2, ContentPaso3;

    private String accion = "0";

    private EditText TextoNombreMarca, TextoNombreCategoria, TextNombreUnidad, TextAbreviaturaUnidad;

    @SuppressLint({"UseRequireInsteadOfGet", "NewApi"})
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tiendas_productos, container, false);

        setHasOptionsMenu(true);

        dialogAgregarProducto = new Dialog(Objects.requireNonNull(getActivity()));
        dialogAgregarMarcaCategoriaUnidad = new Dialog(Objects.requireNonNull(getActivity()));

        TextoBuscar = root.findViewById(R.id.TextoBuscar);

        Buscar = root.findViewById(R.id.Buscar);
        Buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ListaProductos = root.findViewById(R.id.ListaProductos);
        ListaProductos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });

        RefreshProductos = root.findViewById(R.id.RefreshProductos);
        RefreshProductos.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light);

        RefreshProductos.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //
                    }
                }, 1000);
            }
        });

        FloatingActionButton AgregarProducto = root.findViewById(R.id.AgregarProducto);
        AgregarProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AgregarProducto();
            }
        });

        return root;
    }

    @SuppressLint("NewApi")
    private void AgregarProducto(){
        dialogAgregarProducto.setContentView(R.layout.modal_add_producto);
        dialogAgregarProducto.setCancelable(false);

        ImageButton Cerrar = dialogAgregarProducto.findViewById(R.id.Cerrar);
        Cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAgregarProducto.dismiss();
            }
        });

        Paso1 = dialogAgregarProducto.findViewById(R.id.Paso1);
        Paso2 = dialogAgregarProducto.findViewById(R.id.Paso2);
        Paso3 = dialogAgregarProducto.findViewById(R.id.Paso3);

        ContentPaso1 = dialogAgregarProducto.findViewById(R.id.ContentPaso1);
        ContentPaso2 = dialogAgregarProducto.findViewById(R.id.ContentPaso2);
        ContentPaso3 = dialogAgregarProducto.findViewById(R.id.ContentPaso3);

        EditText NombreProducto = dialogAgregarProducto.findViewById(R.id.NombreProducto);

        TextView TextoBuscarMarca = dialogAgregarProducto.findViewById(R.id.TextoBuscarMarca);

        ImageView BuscarMarca = dialogAgregarProducto.findViewById(R.id.BuscarMarca);
        BuscarMarca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        Spinner SpinnerMarca = dialogAgregarProducto.findViewById(R.id.SpinnerMarca);
        SpinnerMarca.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageView AgregarMarca = dialogAgregarProducto.findViewById(R.id.AgregarMarca);
        AgregarMarca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accion = "agregarMarca";

                AgregarMarcaCategoriaUnidad();
            }
        });

        CardView ContinuarPaso1 = dialogAgregarProducto.findViewById(R.id.ContinuarPaso1);
        ContinuarPaso1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //

                Paso1.setBackgroundResource(R.drawable.fondo_image_default);
                Paso1.setTextColor(getResources().getColor(R.color.colorPrimary));

                Paso2.setBackgroundResource(R.drawable.fondo_image_primary);
                Paso2.setTextColor(getResources().getColor(R.color.colorWhite));

                Paso3.setBackgroundResource(R.drawable.fondo_image_default);
                Paso3.setTextColor(getResources().getColor(R.color.colorPrimary));


                ContentPaso1.setVisibility(View.GONE);
                ContentPaso2.setVisibility(View.VISIBLE);
                ContentPaso3.setVisibility(View.GONE);
            }
        });

        TextView NombreProductoText = dialogAgregarProducto.findViewById(R.id.NombreProductoText);

        EditText DescripcionProducto = dialogAgregarProducto.findViewById(R.id.DescripcionProducto);

        EditText TextoBuscarCategoria = dialogAgregarProducto.findViewById(R.id.TextoBuscarCategoria);

        ImageView BuscarCategoria = dialogAgregarProducto.findViewById(R.id.BuscarCategoria);
        BuscarCategoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        Spinner SpinnerCategoria = dialogAgregarProducto.findViewById(R.id.SpinnerCategoria);
        SpinnerCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageView AgregarCategoria = dialogAgregarProducto.findViewById(R.id.AgregarCategoria);
        AgregarCategoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accion = "agregarCategoria";

                AgregarMarcaCategoriaUnidad();
            }
        });

        EditText TextoBuscarUnidad = dialogAgregarProducto.findViewById(R.id.TextoBuscarUnidad);

        ImageView BuscarUnidad = dialogAgregarProducto.findViewById(R.id.BuscarUnidad);
        BuscarUnidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        Spinner SpinnerUnidad = dialogAgregarProducto.findViewById(R.id.SpinnerUnidad);
        SpinnerUnidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageView AgregarUnidad = dialogAgregarProducto.findViewById(R.id.AgregarUnidad);
        AgregarUnidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accion = "agregarUnidad";

                AgregarMarcaCategoriaUnidad();
            }
        });

        EditText PrecioProducto = dialogAgregarProducto.findViewById(R.id.PrecioProducto);

        EditText StockProducto = dialogAgregarProducto.findViewById(R.id.StockProducto);

        CardView ContinuarPaso2 = dialogAgregarProducto.findViewById(R.id.ContinuarPaso2);
        ContinuarPaso2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Paso1.setBackgroundResource(R.drawable.fondo_image_default);
                Paso1.setTextColor(getResources().getColor(R.color.colorPrimary));

                Paso2.setBackgroundResource(R.drawable.fondo_image_default);
                Paso2.setTextColor(getResources().getColor(R.color.colorPrimary));

                Paso3.setBackgroundResource(R.drawable.fondo_image_primary);
                Paso3.setTextColor(getResources().getColor(R.color.colorWhite));


                ContentPaso1.setVisibility(View.GONE);
                ContentPaso2.setVisibility(View.GONE);
                ContentPaso3.setVisibility(View.VISIBLE);
            }
        });

        final EditText Porcentaje = dialogAgregarProducto.findViewById(R.id.Porcentaje);

        final EditText Descuento = dialogAgregarProducto.findViewById(R.id.Descuento);

        CheckBox HabilitarDescuentos = dialogAgregarProducto.findViewById(R.id.HabilitarDescuentos);
        HabilitarDescuentos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Porcentaje.setEnabled(true);
                    Descuento.setEnabled(true);
                } else {
                    Porcentaje.setEnabled(false);
                    Descuento.setEnabled(false);
                }
            }
        });

        CardView TerminarPasos = dialogAgregarProducto.findViewById(R.id.TerminarPasos);
        TerminarPasos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        Objects.requireNonNull(dialogAgregarProducto.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAgregarProducto.show();
    }

    @SuppressLint("NewApi")
    private void AgregarMarcaCategoriaUnidad(){
        dialogAgregarMarcaCategoriaUnidad.setContentView(R.layout.modal_add_unidad_categoria_marca);
        dialogAgregarMarcaCategoriaUnidad.setCancelable(false);

        TextView TituloModal = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.TituloModal);
        TextView TextoContent = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.TextoContent);

        RelativeLayout ContentMarca = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.ContentMarca);
        RelativeLayout ContentCategoria = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.ContentCategoria);
        LinearLayout ContentUnidad = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.ContentUnidad);

        TextoNombreMarca = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.TextoNombreMarca);
        TextoNombreCategoria = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.TextoNombreCategoria);
        TextNombreUnidad = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.TextNombreUnidad);
        TextAbreviaturaUnidad = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.TextAbreviaturaUnidad);

        switch (accion) {
            case "agregarMarca":
                TituloModal.setText(R.string.add_marca);
                TextoContent.setText(R.string.marca);

                ContentMarca.setVisibility(View.VISIBLE);
                ContentCategoria.setVisibility(View.GONE);
                ContentUnidad.setVisibility(View.GONE);
                break;
            case "agregarCategoria":
                TituloModal.setText(R.string.add_categoria);
                TextoContent.setText(R.string.categoria);

                ContentMarca.setVisibility(View.GONE);
                ContentCategoria.setVisibility(View.VISIBLE);
                ContentUnidad.setVisibility(View.GONE);
                break;
            case "agregarUnidad":
                TituloModal.setText(R.string.add_unidad);
                TextoContent.setText(R.string.unidad);

                ContentMarca.setVisibility(View.GONE);
                ContentCategoria.setVisibility(View.GONE);
                ContentUnidad.setVisibility(View.VISIBLE);
                break;
        }

        Button btnCerrar = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAgregarMarcaCategoriaUnidad.dismiss();
            }
        });

        Button btnAplicar = dialogAgregarMarcaCategoriaUnidad.findViewById(R.id.btnAplicar);
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (accion) {
                    case "agregarMarca":

                        break;
                    case "agregarCategoria":

                        break;
                    case "agregarUnidad":

                        break;
                }
            }
        });

        Objects.requireNonNull(dialogAgregarMarcaCategoriaUnidad.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAgregarMarcaCategoriaUnidad.show();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem home = menu.findItem(R.id.item_home);
        home.setVisible(false);
        MenuItem promo = menu.findItem(R.id.item_promociones);
        promo.setVisible(false);
        MenuItem prod = menu.findItem(R.id.item_productos);
        prod.setEnabled(false);
        MenuItem pedido = menu.findItem(R.id.item_pedidos);
        pedido.setVisible(false);
        MenuItem ventas = menu.findItem(R.id.item_ventas);
        ventas.setVisible(false);
        MenuItem config = menu.findItem(R.id.item_configuracion);
        config.setVisible(false);
        MenuItem valor = menu.findItem(R.id.item_valoracion);
        valor.setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }




}