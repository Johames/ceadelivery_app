package com.ceatecsoft.ceadelivery.FragmentsTiendas.Ventas;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ceatecsoft.ceadelivery.Busqueda;
import com.ceatecsoft.ceadelivery.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

public class FragmentVentas extends Fragment {

    Dialog dialogFiltrosVentas;

    private EditText TextoBuscar;

    private ImageView Buscar;

    private SwipeRefreshLayout RefreshVentas;

    private ListView ListaVentas;

    private FloatingActionButton FiltroVentas;

    @SuppressLint({"UseRequireInsteadOfGet", "NewApi"})
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tiendas_ventas, container, false);

        setHasOptionsMenu(true);

        dialogFiltrosVentas = new Dialog(Objects.requireNonNull(getActivity()));

        TextoBuscar = root.findViewById(R.id.TextoBuscar);
        Buscar = root.findViewById(R.id.Buscar);
        RefreshVentas = root.findViewById(R.id.RefreshVentas);
        ListaVentas = root.findViewById(R.id.ListaVentas);
        FiltroVentas = root.findViewById(R.id.FiltroVentas);

        Buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        ListaVentas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        RefreshVentas.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light);

        RefreshVentas.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //
                    }
                }, 1000);
            }
        });

        FiltroVentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MostrarFiltros();
            }
        });

        return root;
    }

    @SuppressLint("NewApi")
    private void MostrarFiltros(){
        dialogFiltrosVentas.setContentView(R.layout.modal_filtros_ventas);
        dialogFiltrosVentas.setCancelable(false);


        Button btnCerrar = dialogFiltrosVentas.findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogFiltrosVentas.dismiss();
            }
        });

        Button btnAplicar = dialogFiltrosVentas.findViewById(R.id.btnAplicar);
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        Objects.requireNonNull(dialogFiltrosVentas.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogFiltrosVentas.show();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem home = menu.findItem(R.id.item_home);
        home.setVisible(false);
        MenuItem promo = menu.findItem(R.id.item_promociones);
        promo.setVisible(false);
        MenuItem prod = menu.findItem(R.id.item_productos);
        prod.setVisible(false);
        MenuItem pedido = menu.findItem(R.id.item_pedidos);
        pedido.setVisible(false);
        MenuItem ventas = menu.findItem(R.id.item_ventas);
        ventas.setEnabled(false);
        MenuItem config = menu.findItem(R.id.item_configuracion);
        config.setVisible(false);
        MenuItem valor = menu.findItem(R.id.item_valoracion);
        valor.setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }




}