package com.ceatecsoft.ceadelivery;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Transition;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.animation.AccelerateInterpolator;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomMenuButton;

import java.util.Timer;
import java.util.TimerTask;

public class PrincipalClientes extends AppCompatActivity {

    int pri = 0;

    BoomMenuButton boom;

    public static final long DURATION_TRANSACTION = 1000;

    BottomNavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_clientes);

        navView = findViewById(R.id.nav_view);

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_explorar, R.id.nav_historial, R.id.nav_notificaciones, R.id.nav_deseos, R.id.nav_perfil)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        boom = findViewById(R.id.BoomMenu);
        int i;
        for (i = 0; i < boom.getPiecePlaceEnum().pieceNumber(); i++){
            if(i == 0){
                HamButton.Builder builder = new HamButton.Builder()
                        .normalImageRes(R.drawable.icono_menu)
                        .normalText("Categorías")
                        .normalTextColor(R.color.colorPrimaryDark)
                        .subNormalText("Revisa las categorías que las tiendas tienen para ti.")
                        .subNormalTextColor(R.color.colorPrimary)
                        .normalColorRes(R.color.colorWhite)
                        .imagePadding(new Rect(15, 15, 15, 15))
                        .rotateImage(true)
                        .shadowEffect(true)
                        .listener(new OnBMClickListener() {
                            @Override
                            public void onBoomButtonClick(int index) {
                                //
                            }
                        });
                boom.addBuilder(builder);
            } else if(i == 1){
                HamButton.Builder builder = new HamButton.Builder()
                        .normalImageRes(R.drawable.icono_filtros)
                        .normalText("Marcas...")
                        .normalTextColor(R.color.colorPrimaryDark)
                        .subNormalText("Revisa las marcas de todos los productos de nuestras tiendas.")
                        .subNormalTextColor(R.color.colorPrimary)
                        .normalColorRes(R.color.colorWhite)
                        .imagePadding(new Rect(15, 15, 15, 15))
                        .rotateImage(true)
                        .shadowEffect(true)
                        .listener(new OnBMClickListener() {
                            @Override
                            public void onBoomButtonClick(int index) {
                                //irPDF();
                            }
                        });
                boom.addBuilder(builder);
            } else if(i == 2){
                HamButton.Builder builder = new HamButton.Builder()
                        .normalImageRes(R.drawable.icono_menu)
                        .normalText("Comparar Precios")
                        .normalTextColor(R.color.colorPrimaryDark)
                        .subNormalText("Busca el precio ideal comparando los precios de todas las tiendas afiliadas.")
                        .subNormalTextColor(R.color.colorPrimary)
                        .normalColorRes(R.color.colorWhite)
                        .imagePadding(new Rect(15, 15, 15, 15))
                        .rotateImage(true)
                        .shadowEffect(true)
                        .listener(new OnBMClickListener() {
                            @Override
                            public void onBoomButtonClick(int index) {
                                //irPDF();
                            }
                        });
                boom.addBuilder(builder);
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal_clientes, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("unchecked")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.item_buscar:

                Transition entrar = new Explode();
                entrar.setDuration(DURATION_TRANSACTION);
                entrar.setInterpolator(new AccelerateInterpolator());

                getWindow().setExitTransition(entrar);

                Intent buscar = new Intent(getApplicationContext(), Busqueda.class);
                startActivity(buscar, ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());

                return true;
            case R.id.item_carrito:

                Transition intro = new Explode();
                intro.setDuration(DURATION_TRANSACTION);
                intro.setInterpolator(new AccelerateInterpolator());

                getWindow().setExitTransition(intro);

                Intent carrito = new Intent(getApplicationContext(), CarritoCompras.class);
                startActivity(carrito, ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        pri = pri + 1;
        if(pri == 2){
            PrincipalClientes.this.finish();
        } else {
            Toast.makeText(getApplicationContext(), R.string.salir, Toast.LENGTH_SHORT).show();
        }
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                pri = 0;
            }
        }, 2000);
    }
}
