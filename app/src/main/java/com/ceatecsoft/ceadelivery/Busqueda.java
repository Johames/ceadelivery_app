package com.ceatecsoft.ceadelivery;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.transition.Fade;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.ceatecsoft.ceadelivery.ModelsTiendas.ModeloHistorialBusqueda;

public class Busqueda extends AppCompatActivity {

    ImageView Regresar, Buscar;

    EditText TextoBuscar;

    String texto = "";

    CardView buscador;

    LinearLayout Space, ContentBusquedasRecientes, ContentVistosRecientes;

    ListView BusquedasRecientes;

    RecyclerView VistosRecientemente, ListaProductosBuscados;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);

        Fade entrar = new Fade(Fade.IN);
        entrar.setDuration(PrincipalClientes.DURATION_TRANSACTION);
        entrar.setInterpolator(new AccelerateInterpolator());

        getWindow().setEnterTransition(entrar);

        ContentBusquedasRecientes = findViewById(R.id.ContentBusquedasRecientes);
        ContentVistosRecientes = findViewById(R.id.ContentVistosRecientes);
        Space = findViewById(R.id.Space);

        buscador =findViewById(R.id.buscador);
        Regresar = findViewById(R.id.Regresar);
        TextoBuscar = findViewById(R.id.TextoBuscar);
        Buscar = findViewById(R.id.Buscar);
        BusquedasRecientes = findViewById(R.id.BusquedasRecientes);
        VistosRecientemente = findViewById(R.id.VistosRecientemente);
        ListaProductosBuscados = findViewById(R.id.ListaProductosBuscados);

        Regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAfterTransition();
            }
        });

        BusquedasRecientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ModeloHistorialBusqueda histBusc = (ModeloHistorialBusqueda) parent.getItemAtPosition(position);

                texto = histBusc.getTexto();
                TextoBuscar.setText(histBusc.getTexto());

                //
            }
        });

        Buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextoBuscar.getText().toString().equals("")){
                    texto = TextoBuscar.getText().toString();

                    ContentBusquedasRecientes.setVisibility(View.GONE);
                    ContentVistosRecientes.setVisibility(View.GONE);
                    Space.setVisibility(View.GONE);
                    ListaProductosBuscados.setVisibility(View.VISIBLE);

                    //
                }
            }
        });



    }






}
